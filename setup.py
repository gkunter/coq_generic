from distutils.core import setup
setup(
  name = 'coq_generic',
  packages = ['coq_generic'],
  version = '0.1',
  description = 'A Coquery corpus module installer. Installs any collection of texts as a corpus.',
  author = 'Gero Kunter',
  author_email = 'corpus.query.tool@gmail.com',
  url = 'https://bitbucket.org/gkunter/coq_generic/',
  download_url = 'https://bitbucket.org/gkunter/coq_generic/get/master.tar.gz', 
  keywords = ['coquery', 'generic', 'linguistic', 'corpus'], 
  license = 'MIT License',
  classifiers = [],
)
